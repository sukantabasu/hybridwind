%--------------------------------------------------------------------------
%DESCRIPTION
%This function computes friction velocity (u*), sensible heat flux (wT),
%and Obukhov length (L) from three-level wind speed data. Stability
%correction functions proposed by Businger and Dyer are utilized. For a
%detailed description of the proposed approach, please refer to: 
%Basu, S. (2018). "A simple recipe for estimating atmospheric stability 
%solely based on surface‐layer wind speed profile", Wind Energy, 21(10), 
%937-941; and
%Basu, S. (2018). "A hybrid profile-gradient approach for the estimation of 
%surface fluxes", Boundary-Layer Meteorology, online first. 
%(https://link.springer.com/article/10.1007/s10546-018-0391-1)

%Note: the function requires MATLAB's optimization toolbox. 

%Input: 
%Z:     an array of 3 sensor heights (m): z1, z2, z3
%U:     an array containing wind speeds (m/s) at heights Z

%Output:
%L:     Obukhov length (m)
%ustar: friction velocity (m/s)
%wT:    surface sensible heat flux (K m/s)

%Example: 
%Z = [5 10 20];
%U = [5 6 7.5];
%[L,ustar,wT,flag] = HybridWind(Z,U);
%Solution: ustar = 0.2886; wT = -0.0509; L = 36.0674
%--------------------------------------------------------------------------

function [L,ustar,wT,flag] = HybridWind(Z,U)

%--------------------------------------------------------------------------
%Constant parameters 
Tref  = 300;    %Reference temperature (K)
kappa = 0.4;    %von Karman constant
g     = 9.81;   %Gravitational acceleration (m/s^2)
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
global z1 z2 z3 RU

z1 = Z(1); z2 = Z(2); z3 = Z(3);
u1 = U(1); u2 = U(2); u3 = U(3);

%Compute wind speed differences (m/s)
du21 = u2 - u1;
du31 = u3 - u1;

%Compute ratio of wind speed differences (RWD) 
RU   = du31/du21;
%Compute RWD for neutral conditions following Eq. (5) of Basu (2018; Wind
%Energy)
RN   = log(z3/z1)/log(z2/z1);
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
%Initialize L with L0 (m)
if RU <= RN             %Neutral and unstable conditions    
    L0 = -1*rand(1);    %Any negative value can be used
else                    %Stable conditions
    L0 = rand(1);       %Any positive value can be used
end

%Specify a stability correction formulation
fun    = @RW_Businger;

%Solve for L for a measured value of RDW
%The 'levenberg-marquardt' algorithm is invoked
options = optimoptions('fsolve','Display','final',...
                       'TolX',1e-10,'TolFun',1e-10,...
                       'MaxFunEvals',1000,'MaxIter',1000,...
                       'StepTolerance',1e-10,'FinDiffType','central',...
                       'Algorithm','levenberg-marquardt');
[L,~,flag] = fsolve(fun,L0,options);
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
%Compute the stability correction terms for heights Z
psim1 = psimBusinger(z1,L);
psim2 = psimBusinger(z2,L);
psim3 = psimBusinger(z3,L);

%Compute the right hand side of Eqs. (3a) and Eqs. (3b) of Basu (2018; Wind
%Energy)
RHS(1) = (log(z2/z1) - psim2 + psim1)/kappa;
RHS(2) = (log(z3/z1) - psim3 + psim1)/kappa;

%Compute the left hand side of Eqs. (3a) and Eqs. (3b) of Basu (2018; Wind
%Energy)
LHS(1) = du21;
LHS(2) = du31;

%Compute friction velocity via linear regression
ustar  = LHS/RHS;
%ustar = LHS*pinv(RHS); %In case the matrix is singular
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
%Compute sensible heat flux using Eq. (7) of Basu (2018; Wind Energy)
wT = -(Tref/(kappa*g))*(ustar^3)/L;
%--------------------------------------------------------------------------

return; 

%--------------------------------------------------------------------------
function f = RW_Businger(L)

global z1 z2 z3 RU

psim1 = psimBusinger(z1,L);
psim2 = psimBusinger(z2,L);
psim3 = psimBusinger(z3,L);

%Compute the numerator of Eq. (4) of Basu (2018; Wind Energy)
Num  = log(z3/z1) - psim3 + psim1;
%Compute the denominator of Eq. (4) of Basu (2018; Wind Energy)
Den  = log(z2/z1) - psim2 + psim1;
%Compute the difference f which will be minimized
f    = RU - Num/Den;
return;
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
function psim = psimBusinger(z,L)
if L <= 0   %Neutral and unstable conditions
    x    = (1-16*z/L)^(1/4);
    psim = 2*log((1+x)/2) + log((1+x^2)/2)...
           - 2*atan(x) + pi/2;
else        %Stable conditions
    psim = -5*z/L;    
end
return;
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
% Copyright, 2018, Sukanta Basu
% 
% The MIT License (MIT)
%
% Permission is hereby granted, free of charge, to any person obtaining a 
% copy of this software and associated documentation files (the "Software"), 
% to deal in the Software without restriction, including without limitation 
% the rights to use, copy, modify, merge, publish, distribute, sublicense, 
% and/or sell copies of the Software, and to permit persons to whom the 
% Software is furnished to do so, subject to the following conditions:

% The above copyright notice and this permission notice shall be included 
% in all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
% FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
% DEALINGS IN THE SOFTWARE.
%--------------------------------------------------------------------------